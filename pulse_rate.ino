#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

#define OLED_RESET     -1
Adafruit_SSD1306 OLED(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

int pulse = A0;
int B;
int x;
int up;
float T;
int bnew;
int bmax;
int down;
int Time;
float bpm;
int Status;
int threshold;
unsigned long start_time;
unsigned long end_time;

void setup() {
  Serial.begin(9600);
  OLED.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  pinMode(pulse, INPUT);

  OLED.clearDisplay();
  OLED.setTextColor(BLACK, WHITE);
  OLED.setCursor(0, 0);
  OLED.setTextSize(2);
  OLED.println("PULSE RATE");
  OLED.display();

}

void loop() {
  //  Serial.print("pulse rate : ");
  //  Serial.println(analogRead(pulse));

  Serial.println("DELAY 3 SEC");
  OLED.clearDisplay();
  OLED.setTextColor(BLACK, WHITE);
  OLED.setCursor(0, 0);
  OLED.setTextSize(2);
  OLED.println("PULSE RATE");
  OLED.display();

  OLED.setTextColor(WHITE, BLACK);
  OLED.setCursor(0, 40);
  OLED.setTextSize(2);
  OLED.print("     3     ");
  OLED.display();
  delay(1000);

  Serial.println("DELAY 2 SEC");
  OLED.clearDisplay();
  OLED.setTextColor(BLACK, WHITE);
  OLED.setCursor(0, 0);
  OLED.setTextSize(2);
  OLED.println("PULSE RATE");
  OLED.display();

  OLED.setTextColor(WHITE, BLACK);
  OLED.setCursor(0, 40);
  OLED.setTextSize(2);
  OLED.print("     2     ");
  OLED.display();
  delay(1000);

  Serial.println("DELAY 1 SEC");
  OLED.clearDisplay();
  OLED.setTextColor(BLACK, WHITE);
  OLED.setCursor(0, 0);
  OLED.setTextSize(2);
  OLED.println("PULSE RATE");
  OLED.display();

  OLED.setTextColor(WHITE, BLACK);
  OLED.setCursor(0, 40);
  OLED.setTextSize(2);
  OLED.print("     1     ");
  OLED.display();
  delay(1000);
  OLED.clearDisplay();

  for (B = 0; B < 80; B++) {
    bnew = analogRead(pulse);

    Serial.print("bnew : ");
    Serial.println(bnew);

    delay(20);
    if (bmax > bnew ) {
      bmax = bmax;
    }
    else if (bmax < bnew) {
      bmax = bnew;
    }
  }
  bmax = bmax - 15;
  threshold = bmax;
  Serial.print("Threshold : ");
  Serial.print(threshold);
  Serial.println(" ");

  delay(1000);

  start_time = millis();

  while (up != 6) {
    x = analogRead(pulse);
    Serial.print("x : ");
    Serial.println(x);

    OLED.clearDisplay();
    OLED.setTextColor(WHITE, BLACK);
    OLED.setCursor(0, 20);
    OLED.setTextSize(2);
    OLED.print("...WAIT...");
    OLED.display();

    if (Status == 0 ) {
      if (threshold < x) {
        up++;
        Serial.print("up : ");
        Serial.println(up);
      }
    }
  }

  end_time = millis();
  T = (end_time - start_time);

  Time = (up / T) * 60000;

  Serial.print("up : ");
  Serial.println(up);

  Serial.print("T : ");
  Serial.println(T);

  Serial.print("Time : ");
  Serial.println(Time);

  OLED.clearDisplay();
  OLED.setTextColor(BLACK, WHITE);
  OLED.setCursor(0, 0);
  OLED.setTextSize(2);
  OLED.println("PULSE RATE");
  OLED.display();

  OLED.setTextColor(WHITE, BLACK);
  OLED.setCursor(0, 20);
  OLED.setTextSize(1);
  OLED.print("pulse rate");
  OLED.display();

  OLED.setTextColor(WHITE, BLACK);
  OLED.setCursor(70, 20);
  OLED.setTextSize(1);
  OLED.print(Time, DEC);
  OLED.display();

  OLED.setTextColor(WHITE, BLACK);
  OLED.setCursor(100, 20);
  OLED.setTextSize(1);
  OLED.print("bpm");
  OLED.display();

  delay(6000);

  if (Time != 0) {
    B           = 0;
    T           = 0;
    x           = 0;
    up          = 0;
    bpm         = 0;
    Time        = 0;
    bnew        = 0;
    bmax        = 0;
    down        = 0;
    Status      = 0;
    threshold   = 0;
    start_time  = 0;
    end_time    = 0;
  }

}
